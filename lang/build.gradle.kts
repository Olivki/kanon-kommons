description = "Provides utilities/extensions for the general Kotlin std-lib"
version = "0.1.0"
extra["packageName"] = "lang"

dependencies {
    compile(kotlin("reflect"))
}