description = "Provides some basic functional constructs and abstract data types for Kotlin"
version = "1.3.0"
extra["packageName"] = "func"

dependencies {
    compile(kotlin("reflect"))
}