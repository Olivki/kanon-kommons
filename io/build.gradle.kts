description = "Provides utilities/extensions for working with IO in Kotlin"
version = "1.1.0"
extra["packageName"] = "io"