include("core", "generator", "collections", "func", "io", "lang", "reflection", "semver")

rootProject.name = "kanon.kommons"

enableFeaturePreview("STABLE_PUBLISHING")