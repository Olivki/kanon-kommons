## kommons.reflection

[![Download](https://api.bintray.com/packages/olivki/kanon.kommons/reflection/images/download.svg)](https://bintray.com/olivki/kanon.kommons/reflection/_latestVersion)

This module contains utilities for working with reflection in Kotlin.

## Installation

Gradle

- Groovy

  ```groovy
  repositories {
      jcenter()
  }
  
  dependencies {
      implementation "moe.kanon.kommons:kommons.reflection:${LATEST_VERSION}"
  }
  ```

- Kotlin

  ```kotlin
  repositories {
      jcenter()
  }
  
  dependencies {
      implementation(group = "moe.kanon.kommons", name = "kommons.reflection", version = "${LATEST_VERSION}")
  }
  ```

Maven

```xml
<dependency>
  <groupId>moe.kanon.kommons</groupId>
  <artifactId>kommons.reflection</artifactId>
  <version>${LATEST_VERSION}</version>
  <type>pom</type>
</dependency>
```